<?php
	class CMSModel{
		public function __construct($db_conn){
			$this->db = $db_conn;
		}
		protected function EscapeString($string){
			return mysqli_real_escape_string($this->db, trim($string));
		}

		//Encrypt password
		protected function EncryptPasssword($password){
			$sql = "SELECT randSalt FROM users";
			$res = $this->db->query($sql);
			$row = mysqli_fetch_array($res);
			$salt = $row['randSalt'];
			$hashed_password = crypt($password, $salt);
			return $hashed_password;
		}
		//Upload an Image
		protected function UploadImage($image_file){
			$image = $image_file['image']['name'];
			$info = new SplFileInfo($image);
			$ext = $info->getExtension();
			if($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg'){
				$image_tmp = $image_file['image']['tmp_name'];
				move_uploaded_file($image_tmp, "../images/$image");
				return $image;
			}
			else{
				$message = "<script> alert('File not supported!!! <br>Please Upload .jpg, .png, or .jpeg images')</script>";
				echo $message;
			}
		}
	}

	/**
	 * Login class
	 */
	class LoginModel extends CMSModel{
		function __construct($db_conn){
			parent::__construct($db_conn);
		}
		public function Login($login_data){
			$username = parent::EscapeString($login_data['username']);
			$password = parent::EscapeString($login_data['password']);
			$sql = "SELECT * FROM users WHERE user_name = '$username'";
			$res= $this->db->query($sql);
			$user_details = [];
			while($row = $res->fetch_assoc()){
				$user_details[] = $row;
			}
			return $user_details;
		}
	}
	
	/**
	 * Categories sub-class
	 */
	class CategoriesModel extends CMSModel{

		function __construct($db_conn){
			parent::__construct($db_conn);
		}

		//Select Categories
		public function SelectCategories(){
			$sql = "SELECT * FROM categories ORDER BY CatId ";
			$cat_title =  $this->db->query($sql);
			$cat_table = [];
			while($row = $cat_title->fetch_assoc()){
				$cat_table[] = $row;
			}
			return $cat_table;
		}

		//Insert Category
		public function InsertCategory($add_category){
			$category_title = $add_category['cat-title'];
			$sql = "INSERT INTO categories(CatTitle) VALUES ('$category_title')";
			if($category_title== "" || empty($category_title)){
				echo "<script>alert('Insert Category Field')</script>";
			}
			else{
				if($this->db->query($sql)){
					
				}
				else{
					$insert_error = "<script>document.querySelector('.insert-error').innerHTML = 'Failed to Insert Category into Database';</script>";
					return $insert_error;
				}
			}
		}
		//Work Later Cannot delete or update a parent row: a foreign key constraint fails
		//Delete Category
		public function DeleteCategory($delete_cat_id){
			$sql = "SELECT post_id FROM posts WHERE post_category_id = $delete_cat_id";
			$res = $this->db->query($sql);
			$category_post = [];
			while($row = $res->fetch_assoc()){
				$category_post[] = $row;
			}
			foreach ($category_post as $value) {
				$post_id = $value['post_id'];
				$sql = "DELETE FROM comments WHERE comment_post_id = $post_id";
				$this->db->query($sql);
			}

			$sql = "DELETE FROM posts WHERE post_category_id = $delete_cat_id";
			$this->db->query($sql);

			$sql = "DELETE FROM categories WHERE CatId = $delete_cat_id";
			$this->db->query($sql);
		}

		//Update Category
		public function UpdateCategory($update_cat_id){
			$updated_cat_title = $update_cat_id['cat-title'];
			$update_cat_id = $update_cat_id['cat-id'];

			$sql = "UPDATE categories SET CatTitle = '$updated_cat_title' WHERE CatId = $update_cat_id";
			$this->db->query($sql);			
		}
	}


	/**
	 * Users Model
	 */
	class UserModel extends CMSModel
	{
		
		function __construct($db_conn){
			parent::__construct($db_conn);
		}

		//InsertUser
		public function InsertUser($user_data){
			$firstname = parent::EscapeString($user_data['firstname']);
			$lastname = parent::EscapeString($user_data['lastname']);
			$user_email = parent::EscapeString($user_data['email']);
			$user_role = parent::EscapeString($user_data['user-role']);
			$username = parent::EscapeString($user_data['username']);
			$password = parent::EscapeString($user_data['password']);

			$password = parent::EncryptPasssword($password);

			$user_image = parent::UploadImage($_FILES);
			if($user_image){

				$sql = "INSERT INTO users(user_firstname, user_lastname, user_email, user_image, user_role, user_name, user_password)";
				$sql .= "VALUES ('$firstname','$lastname','$user_email', '$user_image','$user_role','$username', '$password')";
				$this->db->query($sql);
			}
		}

		public function RegisterUser($user_data){
			$user_email = parent::EscapeString($user_data['email']);
			$username = parent::EscapeString($user_data['username']);
			$password = parent::EscapeString($user_data['password']);
			echo $username;
			// $password = password_hash($password, PASSWORD_BCRYPT, array('cost' => 12));

			$password = parent::EncryptPasssword($password);

			if(!empty($user_email) && !empty($username && !empty($password))){
				$sql = "INSERT INTO users(user_email, user_name, user_password)";
				$sql .= "VALUES ('$user_email','$username', '$password')";
				if($this->db->query($sql)){
					$message = "Registration Successfull!!! Log Into the account to update your details";
					return $message;
				}
				else{
					$message = "Registration Failed!!! Please enter valid data";
					return $message;
				}
			}
			else{
				$message = "Registration Failed!!! Please enter valid data";
				return $message;
			}
		}

		//Deleting User
		public function DeleteUser($user_id){
			$sql = "SELECT * FROM posts WHERE user_id_fk = $user_id";
			$res = $this->db->query($sql);
			while ($row = $res->fetch_assoc()) {
				$post_id = $row['post_id'];
				$sql = "DELETE FROM comments WHERE comment_post_id = $post_id";
				$this->db->query($sql);
			}
			
			$sql = "DELETE FROM posts WHERE user_id_fk = $user_id";
			$this->db->query($sql);

			$sql = "DELETE FROM users WHERE user_id = $user_id";
			$this->db->query($sql);

		}

		//Updating User Role
		public function UpdateUserRole($user_id, $user_role){
			$sql = "UPDATE users SET user_role = '$user_role' WHERE user_id = $user_id";
			$this->db->query($sql);
		}

		public function SelectUpdateUserData($update_user_id){
			$sql = "SELECT * FROM users WHERE user_id = $update_user_id";
			$update_query = $this->db->query($sql);
			$update_user_table = [];
			while($row = $update_query->fetch_assoc()){
				$update_user_table[] = $row;
			}
			return $update_user_table;
		}

		public function SelectUsers(){
			$sql = "SELECT * FROM users";
			$res = $this->db->query($sql);
			$users_table = [];
			while($row= $res->fetch_assoc()){
				$users_table[] = $row;
			}
			return $users_table;
		}

		public function SelectProfileData($username){
			$sql = "SELECT * FROM users WHERE user_name = '$username'";
			$update_query = $this->db->query($sql);
			$update_user_table = [];
			while($row = $update_query->fetch_assoc()){
				$update_user_table[] = $row;
			}
			return $update_user_table;
		}

		//Update User Details
		public function UpdateUserDetails($update_user_data, $update_user_id){
			$firstname = parent::EscapeString($update_user_data['firstname']);
			$lastname = parent::EscapeString($update_user_data['lastname']);
			$user_email = parent::EscapeString($update_user_data['email']);
			$user_role = parent::EscapeString($update_user_data['user-role']);
			$username = parent::EscapeString($update_user_data['username']);
			$password = parent::EscapeString($update_user_data['password']);

			$password = parent::EncryptPasssword($password);

			$user_image = parent::UploadImage($_FILES);

			if(empty($user_image)){
				$select_image_sql = "SELECT user_image FROM users where user_id = $update_user_id";
				$select_image = $this->db->query($select_image_sql);
				while ($row = mysqli_fetch_array($select_image)){
					$user_image = $row['user_image'];
				}
			}
			if($user_image){
				$sql = "UPDATE users SET ";
				$sql .= "user_firstname = '$firstname', ";
				$sql .= "user_lastname = '$lastname', ";
				$sql .= "user_email = '$user_email', ";
				$sql .= "user_role = '$user_role', ";
				$sql .= "user_name= '$username', ";
				$sql .= "user_password = '$password', ";
				$sql .= "user_image = '$user_image' ";
				$sql .= "WHERE user_id = $update_user_id";
				$this->db->query($sql);
			}
		}

		public function UserOnline($session){

			$time = time();
			$time_out_in_seconds = 30;
			$time_out = $time - $time_out_in_seconds;

			$sql = "SELECT * from users_online WHERE session = '$session'";
			$res = $this->db->query($sql);
			$count = mysqli_num_rows($res);

			if($count == NULL)
			{
				$sql = "INSERT INTO users_online(session, time) VALUES ('$session', $time)";
				$this->db->query($sql);
			}
			else
			{
				$sql = "UPDATE users_online SET time = $time WHERE session = '$session'";
				$this->db->query($sql);
			}

			$sql = "SELECT * from users_online WHERE time > $time_out";
			$result = $this->db->query($sql);
			$users_count = mysqli_num_rows($result);

			return $users_count;
		}
	}

	/**
	 * Post Model
	 */
	class PostModel extends CMSModel
	{
		function __construct($db_conn){
			parent::__construct($db_conn);
		}

		//Select Posts
		public function SelectPosts(){
			$sql = "SELECT p.post_id, p.post_title,p.post_author,  p.post_date, p.post_image, p.post_content,
						p.post_tags, p.post_status, p.post_comment_count,p.post_views_count, p.user_id_fk, c.CatTitle
						FROM posts as p 
						LEFT OUTER JOIN categories as c ON p.post_category_id = c.CatId";
			$post_result =  $this->db->query($sql);
			$post_table = [];
			while($row = $post_result->fetch_assoc()){
				$post_table[] = $row;
			}
			return $post_table;
		}

		//Add Post
		public function InsertPost($post_data){

			$post_category_id = $post_data['post-category-id'];
			$post_title = parent::EscapeString($post_data['post-title']);
			$post_author = parent::EscapeString($post_data['post-author']);
			$post_date = date("Y-m-d H:i:s");
			$post_content = parent::EscapeString($post_data['post-content']);
			$post_tags = parent::EscapeString($post_data['post-tags']);
			$post_status = $post_data['post-status'];

			$post_image = parent::UploadImage($_FILES);

			if($post_image){
				$sql = "INSERT INTO posts(post_category_id, post_title, post_date, post_image, post_content, post_tags, post_status, user_id_fk)";
				$sql .= "VALUES ($post_category_id,'$post_title', '$post_date','$post_image','$post_content', '$post_tags', '$post_status', $post_author)";
				$this->db->query($sql);
				$p_id = mysqli_insert_id($this->db);

				return $p_id;
			}
		}

		//Delete Post
		public function DeletePost($delete_post_id){
			$sql = "DELETE FROM comments WHERE comment_post_id = $delete_post_id";
			$this->db->query($sql);

			$sql = "DELETE FROM posts WHERE post_id = $delete_post_id";
			$this->db->query($sql);
		}

		//Select the post that is to be update
		public function SelectUpdatePostData($update_post_id){
			$sql = "SELECT * FROM posts as p LEFT OUTER JOIN users as u ON p.user_id_fk = u.user_id WHERE post_id = $update_post_id";
			$update_query = $this->db->query($sql);
			$update_data_table = [];
			while($row = $update_query->fetch_assoc()){
				$update_data_table[] = $row;
			}
			return $update_data_table;
		}

		//Update Post
		public function UpdatePost($update_post_data, $update_post_id ){
			$post_category_id = $update_post_data['post-category-id'];
			$post_title = parent::EscapeString($update_post_data['post-title']);
			// $post_author = $update_post_data['post-author'];
			$post_content = parent::EscapeString($update_post_data['post-content']);
			$post_tags = parent::EscapeString($update_post_data['post-tags']);
			$post_status = parent::EscapeString($update_post_data['post-status']);

			$post_image = parent::UploadImage($_FILES);
			
			if(empty($post_image)){
				$select_image_sql = "SELECT post_image FROM posts where post_id = $update_post_id";
				$select_image = $this->db->query($select_image_sql);
				while ($row = mysqli_fetch_array($select_image)){
					$post_image = $row['post_image'];
				}
			}
			if($post_image){

				$sql = "UPDATE posts SET ";
				$sql .= "post_category_id = '$post_category_id', ";
				$sql .= "post_title = '$post_title', ";
				// $sql .= "post_author = '$post_author', ";
				$sql .= "post_content = '$post_content', ";
				$sql .= "post_tags = '$post_tags', ";
				$sql .= "post_status= '$post_status', ";
				$sql .= "post_image= '$post_image' ";
				$sql .= "WHERE post_id = $update_post_id";
				if($this->db->query($sql)){
					return  true;
				}
				else{
					die(mysqli_error($this->db));
				}
			}
		}

		//Search posts using tag name
		public function SearchResults($search_data){
			$search = parent::EscapeString($search_data['search']);
			$sql = "SELECT * FROM posts WHERE post_tags LIKE '%$search%'";
			$search_query = $this->db->query($sql);
			$count = mysqli_num_rows($search_query);
			$search_table = [];
			if($count >= 1){
				while($row = $search_query->fetch_assoc()){
					$search_table[] = $row;
				}
			}
			else{
				echo "<script>alert('Data not found')</script>";
				echo "<h1>NO RESULT</h1>";
			}
			return $search_table;
		}

		//SHow Post Data
		public function SelectClickedPostData($p_id){
			$sql = "SELECT p.post_id, p.post_title, p.post_author, p.post_date, p.post_image, p.post_content,
						p.post_tags, p.post_status, p.post_comment_count,p.user_id_fk, c.CatTitle FROM posts as p LEFT OUTER JOIN categories as c ON p.post_category_id = c.CatId WHERE p.post_id = $p_id";
			$post_result =  $this->db->query($sql);
			$post_table = [];
			while($row = $post_result->fetch_assoc()){
				$post_table[] = $row;
			}
			return $post_table;
		} 

		//Show Post Data Based on Category
		public function SelectCategoryPostData($c_id){
			$sql = "SELECT * FROM posts where post_category_id = $c_id";
			$post_result= $this->db->query($sql);
			$post_table = [];
			while($row = $post_result->fetch_assoc()){
				$post_table[] = $row;
			}
			return $post_table;
		}

		//Update post status
		public function UpdatePostStatus($post_id, $post_status){
			$sql = "UPDATE posts SET post_status = '$post_status' WHERE post_id = $post_id";
			$this->db->query($sql);
		}

		public function SelectPostUser(){
			$sql = "SELECT u.user_name, u.user_id
						FROM posts as p INNER JOIN users as u ON p.user_id_fk = u.user_id";
			$user_list = $this->db->query($sql);
			$user_table = [];
			while($row = $user_list->fetch_assoc()){
				$user_table[] = $row;
			}
			return $user_table;
		}

		public function SelectPostByAuthor($post_author){
			$sql = "SELECT p.post_id, p.post_title, u.user_name, p.post_date, p.post_image, p.post_content,
						p.post_tags, p.post_status, p.post_comment_count,p.post_views_count
						FROM posts as p LEFT OUTER JOIN users as u ON u.user_id = p.user_id_fk WHERE user_id_fk = $post_author AND post_status = 'Published'";
			$res = $this->db->query($sql);
			$author_post = [];
			while ($row = $res->fetch_assoc()) {
				$author_post[] = $row;
			}
			return $author_post;
		}

		public function UpdatePostViewCount($post_id){
			$sql = "SELECT post_views_count FROM posts WHERE post_id = $post_id";
			$res = $this->db->query($sql);
			$row = mysqli_fetch_array($res);
			$views_count = $row['post_views_count'];
			$sql = "UPDATE posts SET post_views_count = $views_count+1 WHERE post_id = $post_id";
			$this->db->query($sql);
		}

		public function ResetViewCount($post_id){
			$sql = "UPDATE posts SET post_views_count = 0 WHERE post_id = $post_id";
			$this->db->query($sql);
		}
		public $per_page = 5;
		public function PaggerDivision(){
			$sql = "SELECT * FROM posts";
			$post_result = $this->db->query($sql);
			$raw_count = mysqli_num_rows($post_result);
			$post_count = ceil($raw_count / $this->per_page);
			return $post_count;
		}

		public function PaginatedPost($page){
			if($page == "" || $page ==1){
			    $page_1 =0;
			} else{
			    $page_1 = ($page * $this->per_page) - $this->per_page;
			}
			$sql = "SELECT p.post_id, p.post_title, u.user_name, u.user_id, p.post_date, p.post_image, p.post_content,
						p.post_tags, p.post_status, p.post_comment_count,p.post_views_count, c.CatTitle, p.user_id_fk
						FROM posts as p LEFT OUTER JOIN categories as c ON p.post_category_id = c.CatId LEFT OUTER JOIN users as u ON u.user_id = p.user_id_fk LIMIT $page_1, $this->per_page";
			$post_result =  $this->db->query($sql);
			$post_table = [];
			while($row = $post_result->fetch_assoc()){
				$post_table[] = $row;
			}			
			return $post_table;
		}

		//Count Number of Comments in a post
		public function CountComment(){
			$posts = $this->SelectPosts();
			// print_r($posts);
			foreach ($posts as $post_value){
				$sql = "SELECT p.post_id, c.comment_id FROM posts as p INNER JOIN comments as c ON c.comment_post_id = p.post_id WHERE p.post_id =". $post_value['post_id'];
				$result = $this->db->query($sql);
				$number_of_rows = mysqli_num_rows($result);
				$sql_insert = "UPDATE posts SET post_comment_count = $number_of_rows WHERE post_id =". $post_value['post_id'];
				$this->db->query($sql_insert);
			}
		}

	}

	/**
	 * Comment Model
	 */
	class CommentModel extends CMSModel
	{
		
		function __construct($db_conn){
			parent::__construct($db_conn);
		}

		//Post functionalities


		//Inserting comment
		public function InsertComment($comment_data, $post_id){
			$comment_author = parent::EscapeString($comment_data['comment-author']);
			$comment_email = parent::EscapeString($comment_data['comment-email']);
			$comment_content = parent::EscapeString($comment_data['comment-content']);
			$comment_date = date("Y-m-d H:i:s");

			$sql = "INSERT INTO comments (comment_post_id, comment_author, comment_email, comment_content, comment_date, comment_status)";
			$sql .= "VALUES ($post_id, '$comment_author', '$comment_email', '$comment_content', '$comment_date', 'Unapproved')";
			$this->db->query($sql);
		}

		//Displaying comment in a post
		public function ShowPostComments($post_id){
			$sql = "SELECT * FROM comments WHERE comment_post_id = $post_id AND comment_status = 'Approved'";
			$comment_result = $this->db->query($sql);
			$comment_table = [];
			while($row = $comment_result->fetch_assoc()){
				$comment_table[] = $row;
			}
			return $comment_table;
		}

		//Admin Functionalities

		//Reading Comment in Admin
		public function SelectComments(){
			$sql = "SELECT c.comment_id, c.comment_author, c.comment_email, c.comment_content, c.comment_date, c.comment_status, p.post_title, p.post_id ";
			$sql .= "FROM comments as c LEFT OUTER JOIN posts as p ON c.comment_post_id = p.post_id";
			$res = $this->db->query($sql);
			$comment_table = [];
			while($row = $res->fetch_assoc()){
				$comment_table[] = $row;
			}
			return $comment_table;
		}

		//Display Comment of a specific post
		public function PostComment($post_id){
			$sql = "SELECT c.comment_id, c.comment_author, c.comment_email, c.comment_content, c.comment_date, c.comment_status, p.post_title, p.post_id ";
			$sql .= "FROM comments as c LEFT OUTER JOIN posts as p ON c.comment_post_id = p.post_id WHERE comment_post_id=$post_id";
			$res = $this->db->query($sql);
			$post_comments = [];
			while ($row = $res->fetch_assoc()) {
				$post_comments[] = $row;
			}
			return $post_comments;
		}

		//Delete Comment
		public function DeleteComment($comment_id){
			$sql = "DELETE FROM comments WHERE comment_id = $comment_id";
			$this->db->query($sql);
		}

		//Update comment status
		public function UpdateCommentStatus($comment_id, $comment_status){
			$sql = "UPDATE comments SET comment_status = '$comment_status' WHERE comment_id = $comment_id";
			$this->db->query($sql);
		}
	}

	/**
	 * Dashboard Class
	 */
	class DashboardModel extends CMSModel{
		
		function __construct($db_conn){
			parent::__construct($db_conn);
		}

		//Dashboard Graph
		public function SelectActivePost(){
			$sql = "SELECT * FROM posts WHERE post_status = 'Published'";
			$res = $this->db->query($sql);
			$active_post = [];
			while($row = $res->fetch_assoc()){
				$active_post[] = $row;
			}
			return $active_post;
		}
		public function SelectSubscriber(){
			$sql = "SELECT * FROM users WHERE user_role = 'Subscriber'";
			$res = $this->db->query($sql);
			$subscriber = [];
			while($row = $res->fetch_assoc()){
				$subscriber[] = $row;
			}
			return $subscriber;
		}
		public function SelectUnapprovedComment(){
			$sql = "SELECT comment_status FROM comments WHERE comment_status = 'Unapproved'";
			$res = $this->db->query($sql);
			$unapproved_comment = [];
			while($row = $res->fetch_assoc()){
				$unapproved_comment[] = $row;
			}
			return $unapproved_comment;
		}
		public function SelectDraftPost(){
			$sql = "SELECT * FROM posts WHERE post_status = 'Draft'";
			$res = $this->db->query($sql);
			$draft_post = [];
			while($row = $res->fetch_assoc()){
				$draft_post[] = $row;
			}
			return $draft_post;
		}
	}

?>