
<?php 
    include "homepage_components/functions.php";
?>
    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">
                <?php foreach($post_table as $post_list){ 
                    if($post_list['post_status']=="Published"){ 
                    ?>

                <h1 class="page-header">
                    Page Heading
                    <small>Secondary Text</small>
                </h1>
                <!-- Blog Post -->
                <h2>
                    <a href="post.php?p_id=<?php echo $post_list['post_id'] ?>"><?= $post_list['post_title']?></a>
                </h2>
                <p class="lead">
                    by <a href="index.php"><?= $post_list['post_author']?></a>
                </p>
                <p><span class="glyphicon glyphicon-time"></span> Posted on <?= $post_list['post_date']?></p>
                <hr>
                <a href="post.php?p_id=<?php echo $post_list['post_id'] ?>">
                    <img class="img-responsive" src="images/<?php echo $post_list['post_image']?>" alt="">
                </a>
                <hr>
                <p><?=substr($post_list['post_content'], 0,200) ?></p>
                <a class="btn btn-primary" href="#">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>

                <hr>
                <?php }} ?>
            </div>
            <!-- Blog Categories Well -->
            <?php include "homepage_components/sidebar.php"; ?>

                
            </div>
        </div>
<?php
include "homepage_components/footer.php";
?>