<?php 
    include "../../Config/DBConfig.php";
    include "../../Controller/controller.php";

    session_start();

    $CMSController_obj = new Login($db_conn);
    if (isset($_POST['login'])) {
        $user_details = $CMSController_obj->LoginForm($_POST);
        foreach($user_details as $user_value){
	        $username = $user_value['user_name'];
	        $user_password = $user_value['user_password'];
	        $user_role = $user_value['user_role'];
	        $firstname = $user_value['user_firstname'];
	        $lastname = $user_value['user_lastname'];
	        $email = $user_value['user_email'];
	        $user_string = crypt($_POST['password'], $user_password);
	    }
	    if($username == $_POST['username'] && hash_equals($user_password, $user_string)){
	    	$_SESSION['username'] = $username;
	    	$_SESSION['firstname'] = $firstname;
	    	$_SESSION['lastname'] = $lastname;
	    	$_SESSION['user-role'] = $user_role;
	    	$_SESSION['email'] = $email;
	        header("Location: ../admin");
	    }
	    else{
	        header("Location: ../index.php");
	    }
    }
?>