<?php
	session_start();
	include "homepage_components/header.php";
    include "../Config/DBConfig.php";
    include "../Controller/controller.php";

    //Objects of classes on Controller.php
    $CMSController_obj = new CMSController($db_conn);
    $categories_controller_obj = new CategoriesController($db_conn);
    $post_controller_obj = new PostController($db_conn);
    $comment_controller_obj = new CommentController($db_conn);
    $user_controller_obj = new UserController($db_conn);

    //Show Categories in Navigation Pane
    $cat_list = $categories_controller_obj->ShowCategories();

    //Displaying posts in different pages
    $post_count = $post_controller_obj->PaggerDivision();
    if (isset($_GET['page'])) {
        $page = $_GET['page'];
    }
    else{
        $page = "";
    }
    $post_table = $post_controller_obj->PaginatedPost($page);

    //Search posts using tag name
    $error_message = "";
    if(isset($_POST['submit-search'])){
        if($_POST['search']==""){
            $error_message = "Enter the valid data";
        }
        else{
            $post_table = $post_controller_obj->SearchResults($_POST);
        }
    }

    //Select users of posts
    $user_list = $user_controller_obj->SelectUsers();

    //Show Details of a clicked post
    if(isset($_GET['p_id'])){
        $post_id = $_GET['p_id'];
        $post_table = $post_controller_obj->ShowClickedPostData($post_id);
        $comment_table = $comment_controller_obj->DisplayPostComments($post_id);
        $count = $post_controller_obj->UpdatePostViewCount($post_id);
        //$user_list = $user_controller_obj->SelectUsers();
    }

    //Insert comment in a post
    if(isset($_POST['submit-comment'])){
        if(!empty($_POST['comment-author']) && !empty($_POST['comment-email']) && !empty($_POST['comment-author'])){
            $comment_controller_obj->CreateComment($_POST, $post_id);
        }
        else{
            echo "<script>alert('Field cannot be empty!!');</script>";
        }
    }

    //Show posts of a category
    if(isset($_GET['cat_id'])){
        $c_id = $_GET['cat_id'];
        $post_table = $post_controller_obj->ShowCategoryPostData($c_id);
    }

    //Show all the published post by the user
    if (isset($_GET['author'])) {
        $post_author = $_GET['author'];
        $post_table = $post_controller_obj->SelectPostByAuthor($post_author);
    }

    //Register a user
    if(isset($_POST['register'])){
        $message = $user_controller_obj->RegisterUser($_POST);
    }
    else {
        $message = "";
    }

    //Include Navigation division
    include "homepage_components/navigation.php";
?>