<?php
    
?>
<!-- Blog Sidebar Widgets Column -->
<div class="col-md-4">

    <!-- Blog Search Well -->
    <div class="well">
        <h4>Blog Search</h4>
        <form action="" method="post">
            <div class="input-group">
                <input type="text" name="search" class="form-control">
                <span class="input-group-btn">
                    <button class="btn btn-default" name="submit-search">
                        <span class="glyphicon glyphicon-search"></span>
                    </button>
                </span>
            </div>
            <div class="text-danger" style="color: red;"><?= $error_message ?></div>
        </form> 
        <!-- /.input-group -->
    </div>
    <!--Blog Search Well-->

    <!--Login-->
    <?php if(!(isset($_SESSION['username']))){ ?>
    <div class="login-form well">                
        <form action="homepage_components/login.php" method="post">      
            <h3 style="font-weight: 600; text-align: center;">LOGIN</h3>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Enter Username" name="username">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" placeholder="Enter Password" name="password">  
            </div>
            <div class="form-group">      
                <button type="submit" name="login" class="btn btn-primary">Login</button>
            </div>
        </form>  
    </div>
    <?php }?>
    <!--Login ends here-->


    <div class="well">
        <h4>Blog Categories</h4>
        <div class="row">
            <div class="col-lg-6">
                <ul class="list-unstyled">
                    <?php
                        foreach($cat_list as $cat_title){
                    ?>
                    <li>
                        <a href="category.php?cat_id=<?= $cat_title['CatId']?>"><?= $cat_title['CatTitle']?></a>
                    </li>                    
                    <?php
                        }
                    ?>
                </ul>
            </div>
            <!-- /.col-lg-6 -->
            <!-- <div class="col-lg-6">
                <ul class="list-unstyled">
                    <li><a href="#">Category Name</a>
                    </li>
                    <li><a href="#">Category Name</a>
                    </li>
                    <li><a href="#">Category Name</a>
                    </li>
                    <li><a href="#">Category Name</a>
                    </li>
                </ul>
            </div> -->
            <!-- /.col-lg-6 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- Side Widget Well -->
    <?php include 'widget.php'?>
</div>