
<?php 
    include "homepage_components/functions.php";
?>
    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">
                <?php foreach($post_table as $post_list){?>
                <!-- Blog Post -->
                <h2>
                    <a href="post.php?p_id=<?php echo $post_list['post_id'] ?>"><?= $post_list['post_title']?></a>
                </h2>
                <p class="lead">
                    All posts by <i><?= $post_list['user_name']?></i>
                </p>
                <p><span class="glyphicon glyphicon-time"></span> Posted on <?= $post_list['post_date']?></p>
                <hr>
                <img class="img-responsive" src="images/<?php echo $post_list['post_image']?>" alt="">
                <hr>
                <p><?= $post_list['post_content']?></p>

                <hr>
                <?php } ?>
            </div>
            <!-- Blog Categories Well -->
            <?php include "homepage_components/sidebar.php"; ?>

                
            </div>
        </div>
<?php
include "homepage_components/footer.php";
?>