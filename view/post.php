
<?php 
    include "homepage_components/functions.php";
?>
    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">
                <?php foreach($post_table as $post_list){
                    
                ?>
                <!-- Blog Post -->
                <h2>
                    <a href="post.php?p_id=<?= $post_list['post_id'] ?>"><?= $post_list['post_title']?></a>
                </h2>
                <p class="lead">
                    <?php foreach ($user_list as $user_value){
                        if($user_value['user_id'] == $post_list['user_id_fk']){
                    ?>
                        by <a href="author_posts.php?author=<?= $post_list['user_id_fk']?>"><?= $user_value['user_name']?></a>
                    <?php }}?>
                </p>
                <p><span class="glyphicon glyphicon-time"></span> Posted on <?= $post_list['post_date']?></p>
                <hr>
                <img class="img-responsive" src="images/<?= $post_list['post_image']?>" alt="">
                <hr>
                <p><?= $post_list['post_content']?></p>

                <hr>
                <?php } ?>

                <!-- Blog Comments -->

                <!-- Comments Form -->
                <div class="well">
                    <h4>Leave a Comment:</h4>
                    <p class="error-comment bg-success"></p>
                    <form role="form" action="#" method="post">
                        <div class="form-group">
                            <label for="">Author</label>
                            <input type="text" name="comment-author" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">Email Address</label>
                            <input type="email" name="comment-email" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">Comment</label>
                            <textarea class="form-control" name = "comment-content" rows="3"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary" name="submit-comment">Submit</button>
                    </form>
                </div>

                <hr>

                <!-- Posted Comments -->

                <!-- Comment -->
                <?php foreach($comment_table as $comment_value){ ?>
                <div class="media">
                    <a class="pull-left" href="#">
                        <img class="media-object" src="http://placehold.it/64x64" alt="">
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading"><?= ucwords($comment_value['comment_author'])?>
                            <small>
                                <?php 
                                    $date = date_create($comment_value['comment_date']);
                                    echo date_format($date,"F d, Y ")."at". date_format($date," h:i A");
                                ?>
                                    
                                </small>
                        </h4>
                        <?= $comment_value['comment_content']?>
                    </div>
                </div>
            <?php }?>

            </div>
            <!-- Blog Categories Well -->
            <?php include "homepage_components/sidebar.php"; ?>

                
            </div>
        </div>
<?php
include "homepage_components/footer.php";
?>