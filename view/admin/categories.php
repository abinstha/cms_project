<?php include "admin_components/admin_functions.php"; ?>

<div id="wrapper">
    <!-- Navigation -->
    <?php include 'admin_components/admin_navigation.php' ?>
    <div id="page-wrapper">
        <div class="container-fluid">
            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        Category
                        <small>Add, Update, and Delete</small>
                    </h1>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="insert-error"><?= $insert_error ?></div>
                            <form action="#" method="post">
                                <div class="form-group">
                                    <label for="form-title" class="label-form">Add Category</label>
                                    <input type="text" id="form-title" name="cat-title" class="form-control input-title">
                                    <input type="text" id="form-title" name="cat-id" class=" input-hidden" style="display: none;">
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary modify-category" type="submit" name="add-submit">Add Category</button>
                                </div>
                            </form>
                        </div>
                        <div class="col-sm-6">
                            <table class="table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Category Title</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($cat_list as $cat_title){ ?>
                                    <tr> 
                                        <td class="cat-id"><?= $cat_title['CatId']?></td>
                                        <td class="cat-title"><?= $cat_title['CatTitle']?></td>
                                        <td>
                                            <a href="categories.php?delete_c_id=<?= $cat_title['CatId']?>" onclick ="javascript: return confirm('Are you sure want to delete <?= $cat_title['CatTitle']?> category?')">Delete</a>
                                            <a href="categories.php?c_id=<?= $cat_title['CatId']?>" style="margin-left: 20px" class="update-category">Update</a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<!-- jQuery -->

<?php include 'admin_components/admin_footer.php';?>