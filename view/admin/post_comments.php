<?php
   // if(isset($_GET['delete_comment'])){
   //    $comment_id = $_GET['delete_comment'];
   //    $CMSController_obj->RemoveComment($comment_id);
   // }
  include "admin_components/admin_functions.php";
?>
<div id="wrapper">
  <!-- Navigation -->
  <?php include 'admin_components/admin_navigation.php' ?>
  <div id="page-wrapper">
    <div class="container-fluid">
      <!-- Page Heading -->
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header">
            <?php 
              foreach ($comment_list as $comment_value) {
                echo $comment_value['post_title'];
              }
            ?>
            <small>Comments</small>
          </h1>
          <table class="table table-bordered table-hover">
              <thead>
                  <tr>
                      <th>ID</th>
                      <th>In Response to Post</th>
                      <th>Author</th>
                      <th>Email</th>
                      <th>Date</th>
                      <th>Comment</th>
                      <th>Status</th>
                      <th>Approve</th>
                      <th>Unapprove</th>
                      <th>Delete</th>
                  </tr>
              </thead> 
              <tbody>
                  <?php foreach ($comment_list as $comment_value) {?>
                  <tr>
                      <td><?= $comment_value['comment_id'] ?></td>
                      <td><a href="../post.php?p_id=<?= $comment_value['post_id'] ?>"><?= $comment_value['post_title'] ?></a></td>
                      <td><?= $comment_value['comment_author'] ?></td>
                      <td><?= $comment_value['comment_email'] ?></td>
                      <td><?= $comment_value['comment_date'] ?></td>
                      <td><?= $comment_value['comment_content'] ?></td>
                      <td><?= $comment_value['comment_status'] ?></td>
                      <td><a href="post_comments.php?comment_id=<?= $comment_value['comment_id']?>&comment_status=Approved">Approve</a></td>
                      <td><a href="post_comments.php?comment_id=<?= $comment_value['comment_id']?>&comment_status=Unapproved">Unapprove</a></td>
                      <td><a href="post_comments.php?post_comment_id=<?= $comment_value['post_id']?>&delete_id=<?= $comment_value['comment_id'] ?>">Delete</a></td>
                  </tr>
              <?php }?>
              </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- jQuery -->

<?php include 'admin_components/admin_footer.php';?>