<?php
  include "admin_components/admin_functions.php";
?>
<div id="wrapper">
    <!-- Navigation -->
    <?php include 'admin_components/admin_navigation.php' ?>
    <div id="page-wrapper">
        <div class="container-fluid">
            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        Users 
                    </h1>
                    <?php 
                        if (isset($_GET['source'])) {
                            $source = $_GET['source'];
                        }
                        else{
                            $source = '';
                        }
                        switch ($source) {
                          case 'add_user': 
                            include 'admin_components/add_user.php';
                            break;
                          case 'view_users':
                            include 'admin_components/view_all_users.php';
                            break;
                          case 'edit_user':
                            include 'admin_components/edit_user.php';
                            break;
                          default:
                            include 'admin_components/view_all_users.php';
                            break;
                        }
                    ?>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<!-- jQuery -->

<?php include 'admin_components/admin_footer.php';?>