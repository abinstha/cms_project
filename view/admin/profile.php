<?php
    include "admin_components/admin_functions.php";
?>


<div id="wrapper">
    <!-- Navigation -->
    <?php include 'admin_components/admin_navigation.php' ?>
    <div id="page-wrapper">
        <div class="container-fluid">
            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        <?php foreach ($profile_data as $profile_value) {?>
                        <?= ucfirst($profile_value['user_firstname'])." ".ucfirst($profile_value['user_lastname']) ?>
                        <small>Profile</small>
                    </h1>                   
                    <form action="#" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="firstname">First Name</label>
                            <input type="text" class="form-control" id="firstname" name="firstname" value="<?= $profile_value['user_firstname'] ?>">
                        </div>
                        <div class="form-group">
                            <label for="lastname">Last Name</label>
                            <input type="text" class="form-control" name="lastname" value="<?= $profile_value['user_lastname'] ?>">
                        </div>
                        <div class="form-group">
                            <label for="email">Email Address</label>
                            <input type="email" id="email" class="form-control" name="email" value="<?= $profile_value['user_email'] ?>">
                        </div>
                        <div class="form-group">
                            <label>User Role</label>
                            <select name="user-role" class="form-control" style="width: 300px">
                                <option value="<?= $profile_value['user_role'] ?>"><?= $profile_value['user_role'] ?></option>
                                <?php
                                    if($profile_value['user_role'] == "Admin"){
                                        echo "<option value='Subscriber'>Subscriber</option>";
                                    }
                                    else{
                                        echo "<option value='Admin'>Admin</option>";
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="user-image" style="display: block;">User Image</label>
                            <img width="100" src="../images/<?= $profile_value['user_image'] ?>">
                            <input type="file" id="user-image" class="" name="user-image" value="<?= $profile_value['user_image'] ?>">
                        </div>
                        <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text" id="username" class="form-control" name="username" value="<?= $profile_value['user_name'] ?>">
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" id="password" class="form-control" name="password" value="<?= $profile_value['user_password'] ?>">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary" name="update-profile">Update Profile</button>
                        </div>
                    <?php } ?>
                    </form>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<!-- jQuery -->

<?php include 'admin_components/admin_footer.php';?>