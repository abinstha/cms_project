<?php 
if(isset($_POST['update-post'])){
    $update_post_id = $_GET['p_id'];
    $result = $post_controller_obj->ModifyPost($_POST, $update_post_id);
    if($result == true){
		echo "<p class='bg-success' style='height: 40px; padding: 10px'>Post Updated. <a href = '../post.php?p_id={$_GET['p_id']}'>View Post</a> or <a href='posts.php?view_posts'>Edit More Posts</a></p>";
	}
   }
?>
<form action="" method="post" enctype="multipart/form-data">
	<?php foreach ($update_data as $update_value) {?>
	<div class="form-group">
		<label for="post-title">Post Title</label>
		<input value="<?= $update_value['post_title'] ?>" type="text" class="form-control" id="post-title" name="post-title">
	</div>
	<div class="form-group">
		<label for="post-category-id">Post Category</label>
		<select class="form-control" name="post-category-id" id="post-category-id" style="width: 300px">
			<?php foreach($cat_list as $cat_title){ ?>
				<option value="<?= $cat_title['CatId']?>"><?= $cat_title['CatTitle']?></option>
			<?php }?>
		</select>
	</div>
	<div class="form-group">
		<label for="post-author">Post Author</label>
		<input type="text" class="form-control" name="post-author" value="<?= $update_value['user_name'] ?>" readonly>
	</div>
	<div class="form-group">
		<label for="post-status">Post Status</label>
		<select name="post-status" class="form-control" style="width: 300px">
			<option value="<?= $update_value['post_status'] ?>"><?= $update_value['post_status'] ?></option>
			<?php
				if($update_value['post_status'] == "Published"){
					echo "<option value='Draft'>Draft</option>";
				}
				else{
					echo "<option value='Published'>Publish</option>";
				}
			?>
		</select>
	</div>
	<div class="form-group">
		<label for="post-image">Post Image</label>
		<img width="100" src="../images/<?= $update_value['post_image'] ?>">
		<input type="file" id="post-image" class="" name="image">
	</div>
	<div class="form-group">
		<label for="post-tags">Post Tags</label>
		<input type="text" id="post-tags" class="form-control" name="post-tags" value="<?= $update_value['post_tags'] ?>">
	</div>
	<div class="form-group">
		<label for="post-content">Post Content</label>
		<textarea class="form-control" id="post-content" rows="8" name="post-content" value=""><?= $update_value['post_content'] ?></textarea>
	</div>
	<div class="form-group">
		<button class="btn btn-primary" name="update-post">Update Post</button>
	</div>
	<?php }?>
</form>