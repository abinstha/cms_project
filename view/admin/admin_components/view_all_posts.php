<?php 
    if (isset($_POST['checkBoxArray'])) {
        $bulk_options = $_POST['bulk-options'];
        // die();
        foreach ($_POST['checkBoxArray'] as $checkBoxValue){
            switch ($bulk_options) {
                case 'publish':
                    echo "posts.php?post_id=".$checkBoxValue."&post_status=Published";
                    header("Location: posts.php?post_id=".$checkBoxValue."&post_status=Published");
                    break;
                case 'draft':
                    header("Location: posts.php?post_id=".$checkBoxValue."&post_status=Draft");
                    break;
                case 'delete':
                    header("Location: posts.php?delete_id=".$checkBoxValue);
                    break;
                default:
                    header("Location: posts.php");
                    break;
            }
        }
    }
?>
<form action="" method="post">
    <table class="table table-bordered table-hover">
        <div class="bulk-option-container col-xs-4 col-lg-2 m-5">
            <select name="bulk-options" id="" class="form-control">
                <option value="">Select Options</option>
                <option value="publish">Publish</option>
                <option value="draft">Draft</option>
                <option value="delete">Delete</option>
            </select>
        </div>
        <div class="col-xs-4">
            <input type="submit" class="btn btn-success" value="Apply" name="">
            <a href="posts.php?source=add_post" class="btn btn-primary">Add New</a>
        </div>
        <thead>
            <tr>

                <th><input type="checkbox" class="check-boxes" name=""></th>
                <th>ID</th>
                <th>Author</th>
                <th>Title</th>
                <th>Category</th>
                <th>Status</th>
                <th>Image</th>
                <th>Tags</th>
                <th>Comments</th>
                <th>Date</th>
                <th>View Post</th>
                <th>Edit</th>
                <th>Delete</th>
                <th>Views</th>
            </tr>
        </thead>
        <tbody>
            <form>
            <?php foreach ($post_list as $post_value) {?>
            <tr>
                <td><input type="checkbox" class="check-boxes" name="checkBoxArray[]" value="<?= $post_value['post_id'] ?>"></td>
                <td><?= $post_value['post_id'] ?></td>

                <?php foreach ($user_list as $user_value){
                    if($user_value['user_id'] == $post_value['user_id_fk']){
                ?>
                    <td><?= $user_value['user_name'] ?></td>
                <?php }}?>
                <td><a href="../post.php?p_id=<?= $post_value['post_id'] ?>"><?= $post_value['post_title'] ?></a></td>
                <td><?= $post_value['CatTitle'] ?></td>
                <td><?= $post_value['post_status'] ?></td>
                <td><img width = '100px'src="../images/<?= $post_value['post_image'] ?>"></td>
                <td><?= $post_value['post_tags'] ?></td>
                <td><a href="post_comments.php?post_comment_id=<?= $post_value['post_id']?>"><?= $post_value['post_comment_count'] ?></a></td>
                <td><?= $post_value['post_date'] ?></td>
                <td><a href="../post.php?p_id=<?= $post_value['post_id'] ?>">View Post</a></td>
                <!-- <td><a href="posts.php?post_id=<?= $post_value['post_id']?>&post_status=Published">Publish</a></td> -->
                <td><a href="posts.php?source=edit_post&p_id=<?= $post_value['post_id'] ?>">Edit</a></td>
                <td><a href="posts.php?delete_id=<?= $post_value['post_id'] ?>" class="delete" onclick ="javascript: return confirm('Are you sure want to delete')">Delete</a></td>
                <td><a href="posts.php?reset_view=<?= $post_value['post_id'] ?>" onclick ="javascript: return confirm('Are you sure want to reset views of <?= $post_value['post_title'] ?> post?')"><?= $post_value['post_views_count'] ?></a></td>
            </tr>
            <?php }?>
        </tbody>
    </table>
</form>
<script type="text/javascript">
    
</script>