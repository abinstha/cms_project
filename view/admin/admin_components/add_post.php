<?php
	if (isset($_POST['add-post'])) {

	    $result = $post_controller_obj->AddPost($_POST);
	    // echo $result;
	    // die();
	    if($result){
	         echo "<p class='bg-success' style='height: 40px; padding: 10px'>Post Updated. <a href = '../post.php?p_id={$result}'>View Post</a> or <a href='posts.php?view_posts'>Edit More Posts</a></p>";
	     }
	}
?>
<form action="#" method="post" enctype="multipart/form-data">
	<div class="form-group">
		<label for="post-title">Post Title</label>
		<input type="text" class="form-control" id="post-title" name="post-title">
	</div>
	<div class="form-group">
		<label for="post-category-id">Post Category</label>
		<select class="form-control" name="post-category-id" id="post-category-id" style="width: 300px">
			<?php foreach($cat_list as $cat_title){ ?>
				<option value="<?= $cat_title['CatId']?>"><?= $cat_title['CatTitle']?></option>
			<?php }?>
		</select>
	</div>
	<div class="form-group">
		<label for="post-author">Post Author</label>
		<select class="form-control" name="post-author" id="post-author" style="width: 300px">
			<?php 
				foreach($user_list as $user_details){ 
					if($user_details['user_role'] == 'Admin'){
			?>
				<option value="<?= $user_details['user_id']?>"><?= $user_details['user_name']?></option>
			<?php }}?>
		</select>
	</div> 
	<div class="form-group">
		<label for="post-status">Post Status</label>
		<select name="post-status" class="form-control" style="width: 300px">
			<option value='Draft'>Draft</option>
			<option value='Published'>Publish</option>
		</select>
	</div>
	<div class="form-group">
		<label for="post-image">Post Image</label>
		<input type="file" id="post-image" class="" name="image">
	</div>
	<div class="form-group">
		<label for="post-tags">Post Tags</label>
		<input type="text" id="post-tags" class="form-control" name="post-tags">
	</div>
	<div class="form-group">
		<label for="post-content">Post Content</label>
		<textarea class="form-control" id="post-content" rows="8" name="post-content"></textarea>
	</div>
	<div class="form-group">
		<button class="btn btn-primary" name="add-post">Add Post</button>
	</div>
</form>