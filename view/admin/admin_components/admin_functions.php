<?php
	include "../../Config/DBConfig.php";
  include "../../Controller/controller.php";
  include "admin_components/admin_header.php";

  $session = session_id();

  $CMSController_obj = new CMSController($db_conn);
  $dashboard_controller_obj = new DashboardController($db_conn);
  $categories_controller_obj = new CategoriesController($db_conn);
  $user_controller_obj = new UserController($db_conn);
  $post_controller_obj = new PostController($db_conn);
  $comment_controller_obj = new CommentController($db_conn);


  //Count and display number of items in dashboard graph
  $users_count = $user_controller_obj->UserOnline($session);

  $cat_list = $categories_controller_obj->ShowCategories();
  $post_list = $post_controller_obj->ShowPosts();
  $comment_list = $comment_controller_obj->ShowComments();
  $user_list = $user_controller_obj->SelectUsers();
  $active_post = $dashboard_controller_obj->ShowActivePost();
  $subscriber_list = $dashboard_controller_obj->ShowSubscriber();
  $draft_post = $dashboard_controller_obj->ShowDraftPost();
  $unapproved_comment = $dashboard_controller_obj->ShowUnapprovedComment();

  $active_post_count = sizeof($active_post);
  $subscriber_count = sizeof($subscriber_list);
  $draft_post_count = sizeof($draft_post);
  $unapproved_comment_count = sizeof($unapproved_comment);
  $user_count = sizeof($user_list);
  $comment_count = sizeof($comment_list);
  $post_count = sizeof($post_list);
  $category_count = sizeof($cat_list);


    //Post page

    //Select Post data into the fields
    if (isset($_GET['p_id'])){
      $post_id = $_GET['p_id'];
      $update_data = $post_controller_obj->ShowUpdatePostData($post_id);        
    }

   //Delete a post
   	if (isset($_GET['delete_id'])) {
      $post_id = $_GET['delete_id'];
      $post_controller_obj->RemovePost($post_id);
    }

    //Reset view count to 0
   	if(isset($_GET['reset_view'])){
    	$post_id = $_GET['reset_view'];
    	$post_controller_obj->ResetViewCount($post_id);
    }

    //Update the status of a post
    if(isset($_GET['post_status']) && isset($_GET['post_id'])){
      $post_status = $_GET['post_status'];
    	$post_id = $_GET['post_id'];
    	$post_controller_obj->ModifyPostStatus($post_id, $post_status);
    }

	$user_table = $post_controller_obj->SelectPostUser();
	$post_list = $post_controller_obj->ShowPosts();

	//Comment Page

	//Delete comment 
	if(isset($_GET['delete_comment'])){
      $comment_id = $_GET['delete_comment'];
      $comment_controller_obj->RemoveComment($comment_id);
   	}

   	//Approve or disapprove a status of a comment
   	if(isset($_GET['comment_status']) && isset($_GET['comment_id'])){
      	$comment_status = $_GET['comment_status'];
      	$comment_id = $_GET['comment_id'];
      	$comment_controller_obj->ModifyCommentStatus($comment_id, $comment_status);
    }

   	$comment_list = $comment_controller_obj->ShowComments();


   	//Categories Page

   	//add category
   	$insert_error = "";
   	if(isset($_POST['add-submit'])){
       	$insert_error = $categories_controller_obj->AddCategory($_POST);
   	}

   	//Delete catergory along with posts associated with it
   	if(isset($_GET['delete_c_id'])){
        $cat_id = $_GET['delete_c_id'];
        $categories_controller_obj->RemoveCategory($cat_id);
   	}

   	//Update a category
   	if(isset($_POST['edit-submit'])){
        $categories_controller_obj->ModifyCategory($_POST);
   	}

   	$cat_list = $categories_controller_obj->ShowCategories();

   //post_comments page
   	if(isset($_GET['comment_status']) && isset($_GET['comment_id'])){
      	$comment_status = $_GET['comment_status'];
      	$comment_id = $_GET['comment_id'];
      	$comment_controller_obj->ModifyCommentStatus($comment_id, $comment_status);
    }

   	// $cat_list = $categories_controller_obj->ShowCategories();

   	//Remove a comment
   	if(isset($_GET['delete_id'])){
      	$comment_id = $_GET['delete_id'];
      	$comment_controller_obj->RemoveComment($comment_id);
    }

    //Add comment to a post
   	if (isset($_GET['post_comment_id'])) {
        $post_id = $_GET['post_comment_id'];
        $comment_list = $comment_controller_obj->PostComment($post_id);
    }

    //profile page
    if (isset($_SESSION['username'])) {
    	$username = $_SESSION['username'];
    	$profile_data = $user_controller_obj->ProfileData($username);
  	}

 	  if(isset($_POST['update-profile'])){
    	foreach ($profile_data as $profile_value) {
        $update_user_id = $profile_value['user_id'];
    	}
    	$user_controller_obj->ModifyUserDetails($_POST, $update_user_id);
    	header("Location: profile.php");
  	}


  	//users page
  	if(isset($_POST['add-user'])){
    	$user_controller_obj->AddUser($_POST);
  	}

  	//Delete a user
  	if(isset($_GET['delete_id'])){
    	$delete_user_id = $_GET['delete_id'];
    	if(isset($_SESSION['user-role'])){
      		if($_SESSION['user-role'] == 'Admin'){
        		$user_controller_obj->RemoveUser($delete_user_id);
      		}
    	}
  	}

  	//Load the data of a selected user
  	if(isset($_GET['update_id'])){
    	$update_user_id = $_GET['update_id'];
    	$update_data = $user_controller_obj->ShowUpdateUserData($update_user_id);
  	}

  	//Modify the role of a user
  	if(isset($_GET['user_role']) && isset($_GET['user_id'])){
	    $user_role = $_GET['user_role'];
	    $user_id = $_GET['user_id'];
	    $user_controller_obj->ModifyUserRole($user_id, $user_role);
  	}

  	//Update a user details
  	if(isset($_POST['update-user'])){
	    $update_user_id = $_GET['update_id'];
	    $user_controller_obj->ModifyUserDetails($_POST, $update_user_id);
	    header("Location: users.php");
  	}
  $user_list = $user_controller_obj->SelectUsers();
?>