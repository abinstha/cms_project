
<form action="#" method="post" enctype="multipart/form-data">
	<?php foreach ($update_data as $update_value) {?>
	<div class="form-group">
		<label for="firstname">First Name</label>
		<input type="text" class="form-control" id="firstname" name="firstname" value="<?php echo $update_value['user_firstname'] ?>">
	</div>
	<div class="form-group">
		<label for="lastname">Last Name</label>
		<input type="text" class="form-control" name="lastname" value="<?php echo $update_value['user_lastname'] ?>">
	</div>
	<div class="form-group">
		<label for="email">Email Address</label>
		<input type="email" id="email" class="form-control" name="email" value="<?php echo $update_value['user_email'] ?>">
	</div>
	<div class="form-group">
		<label>User Role</label>
		<select name="user-role" class="form-control" style="width: 300px">
			<option value="<?php echo $update_value['user_role'] ?>"><?php echo $update_value['user_role'] ?></option>
			<?php
				if($update_value['user_role'] == "Admin"){
					echo "<option value='Subscriber'>Subscriber</option>";
				}
				else{
					echo "<option value='Admin'>Admin</option>";
				}
			?>
		</select>
	</div>
	<div class="form-group">
		<label for="user-image" style="display: block;">User Image</label>
		<img width="100" src="../images/<?php echo $update_value['user_image'] ?>">
		<input type="file" id="user-image" class="" name="image">
	</div>
	<div class="form-group">
		<label for="username">Username</label>
		<input type="text" id="username" class="form-control" name="username" value="<?php echo $update_value['user_name'] ?>">
	</div>
	<div class="form-group">
		<label for="password">Password</label>
		<input type="password" id="password" class="form-control" name="password" value="">
	</div>
	<div class="form-group">
		<button class="btn btn-primary" name="update-user">Update User</button>
	</div>
<?php } ?>
</form>