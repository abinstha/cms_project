<table class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>ID</th>
            <th>In Response to Post</th>
            <th>Author</th>
            <th>Email</th>
            <th>Date</th>
            <th>Comment</th>
            <th>Status</th>
            <th>Approve</th>
            <th>Unapprove</th>
            <th>Delete</th>
        </tr>
    </thead> 
    <tbody>
        <?php foreach ($comment_list as $comment_value) {?>
        <tr>
            <td><?= $comment_value['comment_id'] ?></td>
            <td><?= $comment_value['comment_author'] ?></td>
            <td><?= $comment_value['comment_email'] ?></td>
            <td><a href="../post.php?p_id=<?= $comment_value['post_id'] ?>"><?= $comment_value['post_title'] ?></a></td>
            <td><?= $comment_value['comment_date'] ?></td>
            <td><?= $comment_value['comment_content'] ?></td>
            <td><?= $comment_value['comment_status'] ?></td>
            <td><a href="comments.php?comment_id=<?= $comment_value['comment_id']?>&comment_status=Approved">Approve</a></td>
            <td><a href="comments.php?comment_id=<?= $comment_value['comment_id']?>&comment_status=Unapproved">Unapprove</a></td>
            <td><a href="comments.php?delete_comment=<?= $comment_value['comment_id'] ?>">Delete</a></td>
        </tr>
    <?php }?>
    </tbody>
</table>