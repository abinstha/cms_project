
<form action="#" method="post" enctype="multipart/form-data">
	<div class="form-group">
		<label for="firstname">First Name</label>
		<input type="text" class="form-control" id="firstname" name="firstname">
	</div>
	<div class="form-group">
		<label for="lastname">Last Name</label>
		<input type="text" class="form-control" name="lastname">
	</div>
	<div class="form-group">
		<label for="email">Email Address</label>
		<input type="email" id="email" class="form-control" name="email">
	</div>
	<div class="form-group">
		<label>User Role</label>
		<select name="user-role" class="form-control" style="width: 300px">
			<option value="Admin">Admin</option>
			<option value="Subscriber">Subscriber</option>
		</select>
	</div>
	<div class="form-group">
		<label for="user-image">User Image</label>
		<input type="file" id="user-image" class="" name="image">
	</div>
	<div class="form-group">
		<label for="username">Username</label>
		<input type="text" id="username" class="form-control" name="username">
	</div>
	<div class="form-group">
		<label for="password">Password</label>
		<input type="password" id="password" class="form-control" name="password">
	</div>
	<div class="form-group">
		<button class="btn btn-primary" name="add-user">Add User</button>
	</div>
</form>