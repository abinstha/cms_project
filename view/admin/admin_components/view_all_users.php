<table class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>ID</th>
            <th>Username</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Role</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($user_list as $user_value) {?>
        <tr>
            <td><?= $user_value['user_id'] ?></td>
            <td><?= $user_value['user_name'] ?></td>
            <td><?= $user_value['user_firstname'] ?></td>
            <td><?= $user_value['user_lastname'] ?></td>
            <!-- <td><img width = '100px'src="../images/<?= $user_value['post_image'] ?>"></td> -->
            <td><?= $user_value['user_email'] ?></td>
            <td><?= $user_value['user_role'] ?></td>
            <td><a href="users.php?user_id=<?= $user_value['user_id']?>&user_role=Admin">Admin</a></td>
            <td><a href="users.php?user_id=<?= $user_value['user_id']?>&user_role=Subscriber">Subscriber</a></td>
            <td><a href="users.php?source=edit_user&update_id=<?= $user_value['user_id'] ?>">Edit</a></td>
            <td><a href="users.php?delete_id=<?= $user_value['user_id'] ?>">Delete</a></td>
        </tr>
    <?php }?>
    </tbody>
</table>