<?php
  include "admin_components/admin_functions.php";
?>

<div id="wrapper">
    <!-- Navigation -->
    <?php include 'admin_components/admin_navigation.php' ?>
    <div id="page-wrapper">
        <div class="container-fluid">
            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        Post Comments
                    </h1>
                    <?php 
                        if (isset($_GET['source'])) {
                            $source = $_GET['source'];
                        }
                        else{
                            $source = '';
                        }
                        switch ($source) {
                          case 'add_post': 
                            include 'admin_components/add_post.php';
                            break;
                          case 'view_posts':
                            include 'admin_components/view_all_posts.php';
                            break;
                          case 'edit_post':
                            include 'admin_components/edit_post.php';
                            break;
                          default:
                            include 'admin_components/view_all_comments.php';
                            break;
                        }
                    ?>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<!-- jQuery -->

<?php include 'admin_components/admin_footer.php';?>