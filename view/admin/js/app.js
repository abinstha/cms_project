tinymce.init({
	selector:'textarea',
	allow_html_in_named_anchor: false
});
$(document).ready(function(){
	$('#load-screen').delay(700).fadeOut(600, function(){
		$(this).remove();
	});
});
$('.delete').click(function(e){
        confirm("Are you sure you want to delete?");
});
$(".update-category").click(function(e){
        e.preventDefault();
        cat_title_data = $(this).parent().parent().find('td.cat-title').html();
        cat_id_data = $(this).parent().parent().find('td.cat-id').html();
        $(".input-title").val(cat_title_data);
        $(".input-hidden").val(cat_id_data);
        $(".label-form").html("Update Category");
        $(".modify-category").html("Update Category");
        $(".modify-category").attr('name', 'edit-submit');
    });
function loadUserOnline(){
	var users_count = $.ajax({
		type: 'get',
		url: 'index.php',
	});
	users_count.done(function(data){
		users_online = $(data).find('#hidden-span').text();
		$(".users-online").text(users_online);
	});
}

setInterval(function(){
	loadUserOnline();
},500);