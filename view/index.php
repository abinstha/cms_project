
<?php 
    include "homepage_components/functions.php";
?>
    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">
                <?php foreach($post_table as $post_list){                     
                    if($post_list['post_status']=="Published"){ 
                ?>
                <!-- <h1 class="page-header">
                    Page Heading
                    <small>Secondary Text</small>
                </h1> -->
                <!-- Blog Post -->
                <h2>
                    <a href="post.php?p_id=<?= $post_list['post_id'] ?>"><?= $post_list['post_title']?></a>
                </h2>
                <p class="lead">
                    <?php foreach ($user_list as $user_value){
                        if($user_value['user_id'] == $post_list['user_id_fk']){
                    ?>
                        by <a href="author_posts.php?author=<?= $post_list['user_id_fk']?>"><?= $user_value['user_name']?></a>
                    <?php }}?>
                </p>
                <p><span class="glyphicon glyphicon-time"></span> Posted on <?= $post_list['post_date']?></p>
                <hr>
                <a href="post.php?p_id=<?= $post_list['post_id'] ?>">
                    <img class="img-responsive" src="images/<?= $post_list['post_image']?>" alt="">
                </a>
                <hr>
                <p><?=substr($post_list['post_content'], 0,200) ?></p>
                <a class="btn btn-primary" href="post.php?p_id=<?= $post_list['post_id'] ?> ">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>

                <hr>
                <?php }} ?>
            </div>
            <!-- Blog Categories Well -->
            <?php include "homepage_components/sidebar.php"; ?>                
        </div>
        <ul class="pager">
            <?php 
                for($i=1; $i<=$post_count; $i++){
                    if($page == $i){
                        echo "<li class='active-page'><a href='index.php?page=$i'>$i</a></li>";
                    }
                    else{
                        echo "<li><a href='index.php?page=$i'>$i</a></li>";
                    }
                }
            ?>
        </ul>
    </div>
<?php
include "homepage_components/footer.php";
?>