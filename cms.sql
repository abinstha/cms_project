-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 09, 2019 at 10:45 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cms`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `CatId` int(3) NOT NULL,
  `CatTitle` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`CatId`, `CatTitle`) VALUES
(7, 'Bootstrap'),
(2, 'CSS'),
(1, 'HTML'),
(3, 'JavaScript'),
(4, 'jQuery'),
(6, 'MySQL'),
(5, 'PHP');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `comment_id` int(10) UNSIGNED NOT NULL,
  `comment_post_id` int(11) NOT NULL,
  `comment_author` varchar(255) NOT NULL,
  `comment_email` varchar(255) NOT NULL,
  `comment_content` text NOT NULL,
  `comment_date` datetime NOT NULL,
  `comment_status` varchar(150) NOT NULL DEFAULT 'draft'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`comment_id`, `comment_post_id`, `comment_author`, `comment_email`, `comment_content`, `comment_date`, `comment_status`) VALUES
(1, 1, 'Ram Thapa', 'ram@gmail.com', 'I love PHP.', '2019-06-09 09:57:42', 'Approved'),
(2, 1, 'Shyam Maharjan', 'shyam@gmail.com', 'PHP is one of the most easiest programming language to learn in today\'s generation.', '2019-06-09 09:58:35', 'Approved'),
(3, 4, 'Sita Stha', 'sita@hotmail.com', 'No language can replace JavaScript', '2019-06-09 10:00:43', 'Approved'),
(4, 2, 'Nobody', 'nobody@gmail.com', 'I hate this language', '2019-06-09 10:02:02', 'Unapproved');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `post_id` int(11) NOT NULL,
  `post_category_id` int(11) NOT NULL,
  `post_title` varchar(255) NOT NULL,
  `post_author` varchar(255) NOT NULL,
  `post_date` datetime NOT NULL,
  `post_image` text NOT NULL,
  `post_content` text NOT NULL,
  `post_tags` text NOT NULL,
  `post_comment_count` varchar(255) NOT NULL,
  `post_status` varchar(255) NOT NULL DEFAULT 'Draft',
  `post_views_count` int(11) NOT NULL,
  `user_id_fk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`post_id`, `post_category_id`, `post_title`, `post_author`, `post_date`, `post_image`, `post_content`, `post_tags`, `post_comment_count`, `post_status`, `post_views_count`, `user_id_fk`) VALUES
(1, 5, 'PHP tutorial', '', '2019-06-09 08:40:38', 'download.png', 'PHP: Hypertext Preprocessor is a general-purpose programming language originally designed for web development. It was originally created by Rasmus Lerdorf in 1994; the PHP reference implementation is now produced by The PHP Group.&nbsp;', 'php, oop, web', '2', 'Published', 6, 1),
(2, 1, 'HTML tutorial', '', '2019-06-09 08:42:42', 'html.png', 'Hyper Text Markup Language or HTML is the major component of a front-end technology which is used to add content in web pages using markup language. In the full form of HTML, Hypertext illustrates different links between multiple web pages whereas, markup language defines the content or structure of a web pages enclosed within HTML tag. It defines the web pages semantically. This programming language is easy to use, learn, and understand. Without using any other languages, different medias such as image, video, audio can be added to web page. This language is platform independent as it can be used in multiple machines without or with minimal changes in another machine.', 'html, front-end, web', '1', 'Published', 3, 1),
(3, 2, 'CSS Tutorial', '', '2019-06-09 08:46:41', 'css.jpg', 'Cascading Style Sheets, commonly referred ad CSS, is a design language which is used to make pages written in HTML or XML more presentable and interactive. It allows to apply different styles to a HTML elements to illustrate how HTML elements are displayed in a screen. It is used to separate the content of web pages from the presentation or styles. This helps to improve flexibility, content accessibility, and control the different properties of elements. CSS properties can be applied HTML in three ways: inline, embedded, linked. In inline method, CSS is written directly inside the HTML tag whereas in embedded method, CSS is written inside style tag of HTML inside header section. On the other hand, in linked method, CSS is stored in a separate file and is linked to HTML file. Among these methods, highest priority is given to inline CSS by the browser. CSS rule contains two important parts: Selector and a declaration block. Selector selects the HTML element which is to be modified or styled. On the other hand, declaration block consists of one or more declaration which are separated by semi-colon and declaration block starts and end with curly brackets.', 'css, web, front-end', '0', 'Published', 1, 2),
(4, 3, 'JavaScript', '', '2019-06-09 09:17:35', 'javascript.png', 'JavaScript, also known in abbreviated form JS, is one of the most popular object-oriented and cross-platform scripting language which is used to make web pages more interactive by adding different functionalities in it such as fancy and complex animations, popup menus, form/data validation, etc. Overtime, it has evolved to accommodate the demands of web platform. It has different standard library of objects such as RegExp, Array, Boolean, Number. It is supported and adopted by different browser with full concatenation of HTML and CSS. It can be written in two methods: embedded and linked same as CSS but in embedded method, code is written inside script&gt;  element and can be written anywhere inside the document. This code gets executed automatically as soon as Data Object Model (DOM) is loaded. Using JavaScript, we can add event-driven programming into the web page such as triggering certain function when user clicks or presses key. Also, we can add new content, modify existing content, and styles of DOM.', 'JavaScript, front-end, script, web', '1', 'Published', 2, 2),
(5, 4, 'jQuery', '', '2019-06-09 09:19:30', 'jquery.png', 'jQuery is a JavaScript library designed to simplify HTML DOM tree traversal and manipulation, as well as event handling, CSS animation, and Ajax. It is free, open-source software using the permissive MIT License. As of May 2019, jQuery is used by 73% of the 10 million most popular websites.', 'jquery, JavaScript, front-end, web, script, library', '0', 'Draft', 0, 1),
(6, 6, 'MySQL', '', '2019-06-09 09:26:14', 'mysql.png', 'MySQL is an Oracle-backed open source relational database management system RDBMS based on Structured Query Language SQL. MySQL runs on virtually all platforms, including Linux, UNIX, Windows. Although it can be used in a wide range of applications, MySQL is most often associated with web applications and online publishing.MySQL is an important component of an open source enterprise stack called LAMP. LAMP is a web development platform that uses Linux as the operating system, Apache as the web server, MySQL as the relational database management system and PHP as the object-oriented scripting language. (Sometimes Perl or Python is used instead of PHP.)', 'database, mysql', '0', 'Published', 0, 1),
(7, 5, 'PHP', '', '2019-06-09 09:51:01', 'download.png', 'PHP: Hypertext Pre-processor, PHP, is one of the most widely used open source server-side scripting language which is mostly used in web development to enable interaction with databases. In PHP, P stands for preprocessor as the script written in PHP are processed before it is send to the browser. Scripts are generally processed by PHP interpreter integrated in web server or as a Common Gateway Interface (CGI) executable. It is mainly used in web application to manage dynamic content, session tracking, databases, etc. It can be embedded in HTML and also, HTML codes can be written in PHP file. Scripts written in PHP is executed on web server and is end to front-end by converting the result of execution into HTML. The client will be able to see the result of the script but cannot view the actual code and the processes underlying. It supports different databases such as Oracle, MySQL, PostgreSQL, Sybase, and Microsoft SQL Server. PHP code is written in between &lt;?php and ?&gt; (special start and end processing instructions). In PHP, all variable is declared using $ symbol and when declared can be used again and again in the script. Unlike other programming languages, here concatenation of multiple strings is done using &ldquo;.&rdquo; Concatenation operator.', 'php, oop, server-side', '0', 'Published', 0, 2),
(8, 7, 'Bootstrap', '', '2019-06-09 09:56:03', 'bootstrap.png', 'Bootstrap is a free and open-source CSS framework directed at responsive, mobile-first front-end web development. It contains CSS- and JavaScript-based design templates for typography, forms, buttons, navigation and other interface components.', 'bootstrap, html, css, front-end, framework', '0', 'Draft', 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_firstname` varchar(255) NOT NULL,
  `user_lastname` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_image` text NOT NULL,
  `user_role` varchar(255) NOT NULL,
  `randSalt` varchar(255) NOT NULL DEFAULT '$2y$10$thisisforinternshipuse'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `user_password`, `user_firstname`, `user_lastname`, `user_email`, `user_image`, `user_role`, `randSalt`) VALUES
(1, 'admin', '$1$76qMcy3R$xgiYZZLspJ6zVe5gG94aN1', 'admin', 'admin', 'admin@gmail.com', 'user.jpg', 'Admin', '$2y$10$thisisforinternshipuse'),
(2, 'abinstha', '$2y$10$thisisforinternshipuseiC1KxK/HG0B3GBE2A7XU/YdSMlvDb/u', 'Avinna', 'Shrestha', 'abin.crestha01@gmail.com', 'user.jpg', 'Admin', '$2y$10$thisisforinternshipuse'),
(3, 'ismt', '$2y$10$thisisforinternshipuseR54cFJTRfKnh8g7.AH1/Ub/iS68PgWa', 'ISMT', 'College', 'ismt@gmail.com', 'download.jpg', 'Subscriber', '$2y$10$thisisforinternshipuse');

-- --------------------------------------------------------

--
-- Table structure for table `users_online`
--

CREATE TABLE `users_online` (
  `id` int(11) NOT NULL,
  `session` varchar(255) NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_online`
--

INSERT INTO `users_online` (`id`, `session`, `time`) VALUES
(1, 'v6564cjkkfuk2dkainup330o1m', 1559664028),
(2, 'agprotujguekuft2rm4cr5obkp', 1559355329),
(3, '9qv1bba6jmuh91amdbmjvennk6', 1559377689),
(4, 'gn7tkhvpoohtli41gj1btu467k', 1559389723),
(5, '6n1olsusk6hrut3lsr5q46ivea', 1559389834),
(6, 'gbm4mtk336nrshvdq0lni85p3u', 1559389946),
(7, '2ps9hesrsu5vstoihffo5mer46', 1559389990),
(8, 'pb3f4a0d7icucrolmpg5sq1fc9', 1559397031),
(9, '448f4pboqlg1sbhebkr0hkvp2u', 1560068899);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`CatId`),
  ADD UNIQUE KEY `CatTitle` (`CatTitle`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`comment_id`),
  ADD KEY `post_id_fk` (`comment_post_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`post_id`),
  ADD KEY `post_category_id_fkk` (`post_category_id`),
  ADD KEY `user_id_fkk` (`user_id_fk`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `users_online`
--
ALTER TABLE `users_online`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `CatId` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `comment_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users_online`
--
ALTER TABLE `users_online`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `post_id_fk` FOREIGN KEY (`comment_post_id`) REFERENCES `posts` (`post_id`);

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `post_category_id_fkk` FOREIGN KEY (`post_category_id`) REFERENCES `categories` (`CatId`),
  ADD CONSTRAINT `user_id_fkk` FOREIGN KEY (`user_id_fk`) REFERENCES `users` (`user_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
