<?php
	require_once(__DIR__.'/../Model/model.php');
	class CMSController{
		public function __construct($db_conn){
			$this->CMSModel_obj = new CMSModel($db_conn);
		}
	}

	/**
	 * Login Controller
	 */
	class Login extends CMSController{
		
		function __construct($db_conn){
			$this->Login_obj = new LoginModel($db_conn);
		}
		function LoginForm($login_data){
			return $this->Login_obj->Login($login_data);
		}
	}

	/**
	 * Categories Controller
	 */
	class CategoriesController extends CMSController{

		function __construct($db_conn){
			$this->categories_obj = new CategoriesModel($db_conn);
		}

		function ShowCategories(){
			return $this->categories_obj->SelectCategories();
		}
		function AddCategory($category_data){
			return $this->categories_obj->InsertCategory($category_data);
		}
		function RemoveCategory($category_delete_id){
			return $this->categories_obj->DeleteCategory($category_delete_id);
		}
		function ModifyCategory($category_update_id){
			return $this->categories_obj->UpdateCategory($category_update_id);
		}
	}

	/**
	 * User Controlller
	 */
	class UserController extends CMSController
	{
		
		function __construct($db_conn)
		{
			$this->user_model_obj = new UserModel($db_conn);
		}

		function AddUser($user_data){
			return $this->user_model_obj->InsertUser($user_data);
		}

		function RegisterUser($register_data){
			return $this->user_model_obj->RegisterUser($register_data);
		}

		function RemoveUser($user_id){
			return $this->user_model_obj->DeleteUser($user_id);
		}

		function ModifyUserRole($user_id, $user_role){
			return $this->user_model_obj->UpdateUserRole($user_id, $user_role);
		}

		function ShowUpdateUserData($update_user_id){
			return $this->user_model_obj->SelectUpdateUserData($update_user_id);
		}

		function ProfileData($username){
			return $this->user_model_obj->SelectProfileData($username);
		}

		function ModifyUserDetails($update_user_data, $update_user_id){
			return $this->user_model_obj->UpdateUserDetails($update_user_data, $update_user_id);
		}


		function UserOnline($session){
			return $this->user_model_obj->UserOnline($session);
		}
		function SelectUsers(){
			return $this->user_model_obj->SelectUsers();
		}
	}

	/**
	 * Post Controller
	 */
	class PostController extends CMSController
	{		
		function __construct($db_conn){
			$this->post_model_obj = new PostModel($db_conn);
			$this->post_model_obj->CountComment();
		}

		function ShowPosts(){
			return $this->post_model_obj->SelectPosts();
		}

		function AddPost($add_post_data){
			return $this->post_model_obj->InsertPost($add_post_data);
		}

		function RemovePost($delete_post_id){
			return $this->post_model_obj->DeletePost($delete_post_id);
		}

		function ShowUpdatePostData($update_post_id){
			return $this->post_model_obj->SelectUpdatePostData($update_post_id);
		}

		function ModifyPost($update_post_data, $update_post_id ){
			return $this->post_model_obj->UpdatePost($update_post_data, $update_post_id );
		}

		function SearchResults($data){
			return $this->post_model_obj->SearchResults($data);
		}

		function ShowClickedPostData($p_id){
			return $this->post_model_obj->SelectClickedPostData($p_id);
		}

		function ShowCategoryPostData($c_id){
			return $this->post_model_obj->SelectCategoryPostData($c_id);
		}

		function ModifyPostStatus($post_id, $post_status){
			return $this->post_model_obj->UpdatePostStatus($post_id, $post_status);
		}

		function SelectPostUser(){
			return $this->post_model_obj->SelectPostUser();
		}

		function SelectPostByAuthor($post_author){
			return $this->post_model_obj->SelectPostByAuthor($post_author);
		}
		function UpdatePostViewCount($post_id){
			return $this->post_model_obj->UpdatePostViewCount($post_id);
		}
		function ResetViewCount($post_id){
			$this->post_model_obj->ResetViewCount($post_id);
		}
		function PaggerDivision(){
			return $this->post_model_obj->PaggerDivision();
		}
		function PaginatedPost($page){
			return $this->post_model_obj->PaginatedPost($page);
		}
	}

	/**
	 * Comment Controller
	 */
	class CommentController extends CMSController
	{
		
		function __construct($db_conn){
			$this->comment_model_obj = new CommentModel($db_conn);
		}

		function PostComment($post_id){
			return $this->comment_model_obj->PostComment($post_id);
		}

		function CreateComment($comment_data, $post_id){
			return $this->comment_model_obj->InsertComment($comment_data, $post_id);
		}

		function ShowComments(){
			return $this->comment_model_obj->SelectComments();
		}

		function RemoveComment($comment_id){
			$this->comment_model_obj->DeleteComment($comment_id);
		}

		function ModifyCommentStatus($comment_id, $comment_status){
			return $this->comment_model_obj->UpdateCommentStatus($comment_id, $comment_status);
		}

		function DisplayPostComments($post_id){
			return $this->comment_model_obj->ShowPostComments($post_id);
		} 

	}

	/**
	 * Dashboard Controller
	 */
	class DashboardController extends CMSController
	{
		
		function __construct($db_conn){
			$this->dashboard_obj = new DashboardModel($db_conn);
		}

		//Dashboard Page
		function ShowActivePost(){
			return $this->dashboard_obj->SelectActivePost();
		}
		function ShowSubscriber(){
			return $this->dashboard_obj->SelectSubscriber();
		}
		function ShowUnapprovedComment(){
			return $this->dashboard_obj->SelectUnapprovedComment();
		}
		function ShowDraftPost(){
			return $this->dashboard_obj->SelectDraftPost();
		}
	}
?>